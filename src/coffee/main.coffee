require [], () ->

  ###
  Study.1
  ###

  anim1 = ->
    $("#box1").animate
      left: 300, {
      duration: 1000
      easing: 'linear'
      step: (now, fx)->
        opacity = 1 - (now / 300)
        $("#box2").css "opacity" : opacity
      complete: ->
      }
  $("#btn_anim1").on "click", anim1

  ###
  Study.2
  ###
  anim2 = ->
    $("#box3").animate
      scale: 1, {
        duration: 500
        easing: 'easeOutExpo'
        step: (now, fx)->
          scaleVal = now
          fx.start = 0.1
          console.log fx.pos
          if fx.pos > 0.8
            $("#box3").addClass "on"
          else
            $("#box3").removeClass "on"
          $("#box3").css "transform" : "scale(" + scaleVal + ")"
      }

  $("#btn_anim2").on "click", anim2

  ###
  Study.3
  ###
  anim3 = ->
    $("#sprites").animate
      top: 0, {
        duration: 600
        easing: 'easeInOutExpo'
        step: (now, fx)->
          pos = Math.floor(fx.pos * 29) * 178 * -1
          $("#sprites").css
            "background-position": "0 " + pos + "px"
      }

  $("#btn_anim3").on "click", anim3
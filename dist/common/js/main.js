// Sun, 15 Dec 2013 12:44:13 GMT
(function() {
(function() {
  require([], function() {
    /*
    Study.1
    */

    var anim1, anim2, anim3;
    anim1 = function() {
      return $("#box1").animate({
        left: 300
      }, {
        duration: 1000,
        easing: 'linear',
        step: function(now, fx) {
          var opacity;
          opacity = 1 - (now / 300);
          return $("#box2").css({
            "opacity": opacity
          });
        },
        complete: function() {}
      });
    };
    $("#btn_anim1").on("click", anim1);
    /*
    Study.2
    */

    anim2 = function() {
      return $("#box3").animate({
        scale: 1
      }, {
        duration: 500,
        easing: 'easeOutExpo',
        step: function(now, fx) {
          var scaleVal;
          scaleVal = now;
          fx.start = 0.1;
          console.log(fx.pos);
          if (fx.pos > 0.8) {
            $("#box3").addClass("on");
          } else {
            $("#box3").removeClass("on");
          }
          return $("#box3").css({
            "transform": "scale(" + scaleVal + ")"
          });
        }
      });
    };
    $("#btn_anim2").on("click", anim2);
    /*
    Study.3
    */

    anim3 = function() {
      return $("#sprites").animate({
        top: 0
      }, {
        duration: 600,
        easing: 'easeInOutExpo',
        step: function(now, fx) {
          var pos;
          pos = Math.floor(fx.pos * 29) * 178 * -1;
          return $("#sprites").css({
            "background-position": "0 " + pos + "px"
          });
        }
      });
    };
    return $("#btn_anim3").on("click", anim3);
  });

}).call(this);

define("main", function(){});
} ());